'use strict';

const vendorJs = [
    './node_modules/jquery/dist/jquery.js',
    './node_modules/tether/dist/js/tether.js',
    './node_modules/bootstrap/dist/js/bootstrap.js'

];

const vendorStyles = [
    './node_modules/bootstrap/dist/css/bootstrap.css',
];

var fileinclude = require('gulp-file-include'),
  gulp = require('gulp'),
  del = require('del'),
  concat = require('gulp-concat'),
  sass = require('gulp-sass'),
  clean = require('gulp-clean'),
  server = require('browser-sync');

server.create();

gulp.task('serve', () => {
    server.init({
        files: ['./dist/**/*'],
        port: 4000,
        server: {
            baseDir: './dist'
        }
    });
});

gulp.task('del', function() {
  return del('dist');
});

gulp.task('clean', function () {
  return gulp.src('./dist')
    .pipe(clean());
});
 
gulp.task('fileinclude', function() {
  gulp.src(['./src/html/*.html'])
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file'
    }))
    .pipe(gulp.dest('./dist/'));
});

gulp.task('update:html', ['clean:html'], function() {
  return gulp.src(['./src/html/*.html'])
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file'
    }))
    .pipe(gulp.dest('./dist/'));
});

gulp.task('copy', ['clean'], function() {
    gulp.src('./src/images/**')
        .pipe(gulp.dest('./dist/images/'));

    gulp.src(['./src/html/*.html'])
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file'
    }))
    .pipe(gulp.dest('./dist/'));

    gulp
        .src(['src/sass/*.scss', '!src/sass/_*.scss'])
        .pipe(sass({outputStyle: 'nested' }))
        .pipe(gulp.dest('./dist/styles/'));

    gulp
        .src(['src/js/*.js'])
        .pipe(gulp.dest('./dist/js/'));

    gulp.src(vendorJs)
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest('./dist/vendor'));

    return gulp.src(vendorStyles)
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest('./dist/vendor'));
});



gulp.task('styles', function() {
    return gulp
        .src(['src/sass/*.scss', '!src/sass/_*.scss'])
        .pipe(sass({outputStyle: 'nested', }))
        .pipe(gulp.dest('./dist/styles/'));
});

gulp.task('javascript', function() {
    return gulp
        .src(['src/js/*.js'])
        .pipe(gulp.dest('./dist/js/'));
});

gulp.task('clean:styles', function() {
    return del('./dist/styles/**/**');
});

gulp.task('clean:js', function() {
    return del('./dist/js/**/**');
});

gulp.task('clean:html', function() {
    return del('./dist/html/**/**');
});

gulp.task('vendor', function() {
    gulp.src(vendorJs)
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest('./dist/vendor'));

    return gulp.src(vendorStyles)
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest('./dist/vendor'));
});

gulp.task('build', [
  'copy',
]);

// gulp.task('update:html', ['images'], function() {
//     gulp.src(['./src/html/*.html'])
//     .pipe(fileinclude({
//       prefix: '@@',
//       basepath: '@file'
//     }))
//     .pipe(gulp.dest('./dist/'));

//     gulp.src(vendorJs)
//         .pipe(concat('vendor.js'))
//         .pipe(gulp.dest('./dist/vendor'));

//     return gulp.src(vendorStyles)
//         .pipe(concat('vendor.css'))
//         .pipe(gulp.dest('./dist/vendor'));
// });

gulp.task('watch', function() {
    gulp.watch(['./src/html/**/*'], ['update:html']);
    gulp.watch('./src/sass/**/*.scss', ['clean:styles', 'styles']);
    gulp.watch('./src/js/**/*.js', ['clean:js', 'javascript']);
});

gulp.task('default', [
    'serve',
    'watch'
]);