<!-- Modal -->
<div class="modal  fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content remove-modal  ">
            <h3>Are you sure you want to remove <b>Drill</b> from your cart?</h3>
            <div class="custom-form__control">
                <div class="btn-container inverse">
                    <button class="btn-text">Confirm</button>
                </div>
                <div class="btn-container">
                    <button class="color-grey btn-text" type='submit'>Cansel</button>
                </div>
            </div>
        </div>
    </div>
</div>