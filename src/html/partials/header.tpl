<header>
    <div class="container">
        <div class="row align-items-center header__height justify-content-between">
            <div class="col ">
                <a href="../index.html" >
                    <img src="./images/ivaldi_logo_black.svg" alt="">
                </a>
            </div>
            <div class="col-md-6 col-sm-12 col-lg-4">
                <div class="row  align-items-center">
                    <div class="account">
                        <div class=" col ">
                            <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Michael Nino Evensen
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="col basket">
                     <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>