var btn = $('#toggle-cart');
var cart = $('#cart');


if(btn!=null)
btn.on('click', function () {
   cart.toggleClass('transformed');
});

var table = $('#table-container');
if(table!=null){
    table.on('click', function (e) {
        if(e.target.getAttribute('class')==='remove'){
            $('#myModal').modal('toggle');
        }
    })
}

var switcher = $("#switcher");
if(switcher!=null){
    switcher.on('click', function () {
        $('#grid').toggleClass('active');
        $('#list').toggleClass('active');

        if($('#list').hasClass('active')){
            $('#view-switcher').removeClass('category-list').addClass('category-grid');

        }else{
            $('#view-switcher').removeClass('category-grid').addClass('category-list');
        }
    });
}
